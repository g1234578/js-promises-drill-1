/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/
const fs = require('fs').promises;

const directoryPath = './JSONRandomFiles'

function createRandomfiles(){
    return fs.mkdir(directoryPath, { recursive: true })
    .then(() => {
        console.log(`Directory '${directoryPath}' created.`);
        const promises = [];
        for (let index = 0; index < 10; index++) {
            promises.push(
                fs.writeFile(`./JSONRandomFiles/file${index}.json`, JSON.stringify(`file ${index}`))
                    .then(() => console.log(`File file${index}.json created successfully`))
                    .catch(error => console.error(`Error creating file file${index}.json:`, error))
            );
        }
        return Promise.all(promises);
    })
    .then(() => console.log("All files created successfully"))
    .catch(err => console.error(`Error creating directory: ${err.message}`));

}


function deleteRandomFiles(){
    const promises = [];
    for(let index = 0 ; index < 10 ; index++){
        promises.push(fs.unlink(`./JSONRandomFiles/file${index}.json`)
        .then(()=> console.log("Deleted successfully"))
        .catch(()=> console.log("Error in deleting")))
    }
    return Promise.all(promises);
}

createRandomfiles();

module.exports = {createRandomfiles,deleteRandomFiles};



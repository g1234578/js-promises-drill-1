const { readFile, writeFile, appendFile, deleteFile } = require("./operations");
const fs = require("fs").promises;

/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

function performOperations() {
  //1.
  readFile("lipsum_2.txt").then((data) => {
    //console.log(data);
    //2.
    let upperCaseText = data.toUpperCase();
    //console.log(upperCaseText);
    return (
      writeFile("upperCaseText.txt", upperCaseText)
        .then(() => writeFile("filesName.txt", "upperCaseText.txt\n"))
        .then(() => {
          return upperCaseText;
        })

        // Step 3: Convert upperCaseData to lowercase, split into sentences, and write to sentences.txt

        .then((upperCaseText) => {
          const lowerCaseText = upperCaseText.toLowerCase();
          //console.log(lowerCaseText);
          const paragraph = lowerCaseText.split("\n\n");
          let totalSentences = "";
          //console.log(paragraph);
          paragraph.forEach((para) => {
            const splitedPara = para.trim();
            const sentence = splitedPara.split(".");
            totalSentences += sentence.join("\n");
          });

          //console.log(totalSentences);

          return writeFile("sentences.txt", totalSentences)
            .then(() => appendFile("filesName.txt", "sentences.txt\n"))
            .then(() => {
              return totalSentences;
            });
        })

        // 4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt

        .then((totalSentences) => {
          const sortedSentences = totalSentences.trim().split("\n").sort();
          const sortedText = sortedSentences.join("\n");
          //console.log(sortedText);
          return writeFile("sortedSentences.txt", sortedText)
            .then(() => appendFile("filesName.txt", "sortedSentences.txt"))
            .then(() => {
              return sortedSentences;
            });
        })

        //5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.

        .then(() => {
          return readFile("filesName.txt")
            .then((data) => {
              const promises = [];

              const files = data.trim().split("\n");
              const deletedFiles = files.map((file) => {
                return deleteFile(file);
              });

              Promise.all(deletedFiles);
            })

            .then(() => console.log("All operations performed successfully"))
            .catch(() =>
              console.log("Error detected while performing the operations")
            );
        })
    );
  });
}

module.exports = performOperations;

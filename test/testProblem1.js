const { createRandomfiles, deleteRandomFiles } = require('../problem1');

createRandomfiles()
    .then(() => {
        console.log("Success");
        return deleteRandomFiles();
    })
    .then(() => {
        console.log("Deleted");
    })

    .catch((error) => {
        console.error("Error:", error);
    });


